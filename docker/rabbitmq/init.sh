#!/bin/sh

#https://stackoverflow.com/questions/30747469/how-to-add-initial-users-when-starting-a-rabbitmq-docker-container
# Create Rabbitmq user
( sleep 5 ; \
rabbitmqctl delete_user guest ; \
rabbitmqctl add_user $RABBITMQ_DEFAULT_USER $RABBITMQ_DEFAULT_PASS 2>/dev/null ; \
rabbitmqctl set_user_tags $RABBITMQ_DEFAULT_USER administrator ; \
rabbitmqctl set_permissions $RABBITMQ_DEFAULT_USER ".*" ".*" ".*" ; \
echo "*** User '$RABBITMQ_CHAT_USERNAME' with password '$RABBITMQ_CHAT_USER_PASS' completed. ***" ; \
rabbitmqctl add_user $RABBITMQ_CHAT_USERNAME $RABBITMQ_CHAT_USER_PASS 2>/dev/null ; \
rabbitmqctl set_permissions $RABBITMQ_CHAT_USERNAME "^$" "^$" "chat_.*" ; \
echo "*** User '$RABBITMQ_CHAT_USERNAME' with password '$RABBITMQ_CHAT_USER_PASS' completed. ***" ; \
echo "*** Log in the WebUI at port 15672 (example: http:/localhost:15672) ***") &

# $@ is used to pass arguments to the rabbitmq-server command.
# For example if you use it like this: docker run -d rabbitmq arg1 arg2,
# it will be as you run in the container rabbitmq-server arg1 arg2
rabbitmq-server $@
