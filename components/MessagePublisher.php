<?php
/**
 * Created by PhpStorm.
 * User: yablonsky
 * Date: 27.05.19
 * Time: 17:29
 */

namespace app\components;

use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;

class MessagePublisher
{
    private $channel;
    private $exchange = 'chat';

    public function __construct()
    {
        $connection = new AMQPStreamConnection(
            'rabbitmq',
            5672,
            env('RABBITMQ_DEFAULT_USER'),
            env('RABBITMQ_DEFAULT_PASS')
        );
        $this->channel = $connection->channel();
        $this->channel->exchange_declare($this->exchange, 'direct', false, true, false);
    }

    public function publish($text, $to, $from)
    {
        $text = json_encode(['text' => $text, 'from' => $from]);
        $message = new AMQPMessage($text);
        return $this->channel->basic_publish($message, $this->exchange, $to);
    }

    public function generateQueue($userId)
    {
        $queue = 'chat_' . \Yii::$app->security->generateRandomString();
        $this->channel->queue_declare($queue, false, false, false, false, false, ['x-expires' => ["I", 60 * 1000]]);
        $this->channel->queue_bind($queue, 'chat', $userId);
        return $queue;
    }

}
