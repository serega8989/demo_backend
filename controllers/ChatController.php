<?php

namespace app\controllers;

use app\components\MessagePublisher;
use app\components\Controller;
use app\models\User;

class ChatController extends Controller
{
    public function behaviors()
    {
        $behaviors = parent::behaviors();

        $behaviors['authenticator'] = [
            'class' => \yii\filters\auth\HttpHeaderAuth::className(),
            'except' => ['options']
        ];

        return $behaviors;
    }

    public function actionGetQueue()
    {
        $publisher = new MessagePublisher();
        return ['queue' => $publisher->generateQueue(\Yii::$app->user->id)];
    }

    public function actionSendMessage()
    {
        $body = \Yii::$app->request->bodyParams;
        $publisher = new MessagePublisher();
        $from = \Yii::$app->user->id;
        $publisher->publish($body['text'], $body['userId'], $from);
        \Yii::$app->response->statusCode = 200;
    }

    public function actionUsers()
    {
        $users = User::getAll();
        $currentUserId = \Yii::$app->user->id;

        $result = [];
        foreach ($users as $id => $user) {
            if ($currentUserId != $id) {
                $result[] = ['id' => $id, 'username' => $user['username']];
            }
        }

        return $result;
    }

}
