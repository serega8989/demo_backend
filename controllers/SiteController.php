<?php
namespace app\controllers;

use app\models\LoginForm;
use app\components\Controller;

class SiteController extends Controller
{
    public function actions()
    {
        $actions = [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ]
        ];

        return array_merge(parent::actions(), $actions);
    }

    public function actionLogin()
    {
        $model = new LoginForm();

        if ($model->load(\Yii::$app->request->bodyParams, '') && $model->validate()) {
            return $model->getUserInfo();
        }

        return $model;
    }
}
