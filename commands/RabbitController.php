<?php
/**
 * Created by PhpStorm.
 * User: sergey
 * Date: 03.08.19
 * Time: 21:00
 */

namespace app\commands;

use app\components\MessagePublisher;
use yii\console\Controller;

class RabbitController extends Controller
{
    public function actionCreateQueue($userId)
    {
        $publisher = new MessagePublisher();
        echo $publisher->generateQueue($userId) . PHP_EOL;
    }

    public function actionPublish($message, $to, $from)
    {
        $publisher = new MessagePublisher();
        echo $publisher->publish($message, $to, $from) . PHP_EOL;
    }
}
